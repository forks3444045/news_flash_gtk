use glib::subclass;
use glib::{clone, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CompositeTemplate, Label, Widget};
use once_cell::sync::Lazy;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/add_dialog/error.blp")]
    pub struct AddErrorWidget {
        #[template_child]
        pub error_message: TemplateChild<Label>,
        #[template_child]
        pub try_again_button: TemplateChild<Button>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddErrorWidget {
        const NAME: &'static str = "AddErrorWidget";
        type ParentType = Box;
        type Type = super::AddErrorWidget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddErrorWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| vec![Signal::builder("try-again").build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for AddErrorWidget {}

    impl BoxImpl for AddErrorWidget {}
}

glib::wrapper! {
    pub struct AddErrorWidget(ObjectSubclass<imp::AddErrorWidget>)
        @extends Widget, Box;
}

impl Default for AddErrorWidget {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl AddErrorWidget {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        imp.try_again_button.connect_clicked(
            clone!(@weak self as widget => @default-panic, move |_button| widget.emit_by_name::<()>("try-again", &[])),
        );
    }

    pub fn set_error(&self, error: &str) {
        let imp = self.imp();
        imp.error_message.set_text(error);
    }
}
