#!/bin/sh
# Source: https://gitlab.gnome.org/World/podcasts/

glib-compile-resources --sourcedir=../data/resources/ --target=../data/resources/gresource_bundles/icons.gresource ../data/resources/icons.gresource.xml
glib-compile-resources --sourcedir=../data/resources/ --target=../data/resources/gresource_bundles/styles.gresource ../data/resources/styles.gresource.xml