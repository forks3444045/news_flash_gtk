use crate::enclosure_button::EnclosureButton;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, Image, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, ActionRow, PreferencesRow};
use news_flash::models::Enclosure;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/enclosures/row.blp")]
    pub struct EnclosureRow {
        #[template_child]
        pub invis_button: TemplateChild<Button>,
        #[template_child]
        pub icon: TemplateChild<Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EnclosureRow {
        const NAME: &'static str = "EnclosureRow";
        type ParentType = ActionRow;
        type Type = super::EnclosureRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EnclosureRow {}

    impl WidgetImpl for EnclosureRow {}

    impl ListBoxRowImpl for EnclosureRow {}

    impl ActionRowImpl for EnclosureRow {}

    impl PreferencesRowImpl for EnclosureRow {}
}

glib::wrapper! {
    pub struct EnclosureRow(ObjectSubclass<imp::EnclosureRow>)
        @extends Widget, PreferencesRow, ActionRow;
}

impl Default for EnclosureRow {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl EnclosureRow {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self, enclosure: &Enclosure, title: &str) {
        let imp = self.imp();

        if let Some(title) = &enclosure.title {
            self.set_title(title);
        } else {
            self.set_title(title);
        }

        let icon_name = if let Some(mime) = enclosure.mime_type.as_ref() {
            if mime.starts_with("image") {
                "image-x-generic-symbolic"
            } else if mime.starts_with("video") {
                "video-x-generic-symbolic"
            } else if mime.starts_with("audio") {
                "audio-x-generic-symbolic"
            } else {
                "globe-alt-symbolic"
            }
        } else {
            "globe-alt-symbolic"
        };

        let url_string = enclosure.url.to_string();
        let url_shortened = url_string.chars().take(35).chain("...".chars()).collect::<String>();

        imp.icon.set_icon_name(Some(icon_name));
        self.set_subtitle(&url_shortened);
        self.set_tooltip_text(Some(&url_string));

        imp.invis_button.connect_activate(clone!(
            @strong enclosure => @default-panic, move |_button| EnclosureButton::launch_enclosure(&enclosure)));
    }
}
