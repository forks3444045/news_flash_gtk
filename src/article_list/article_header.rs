use crate::util::DateUtil;

use super::models::{ArticleGObject, GDateTime};
use chrono::Utc;
use gtk4::{subclass::prelude::*, CompositeTemplate, Label};
use std::cell::Cell;

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/article_list/section_header.blp")]
    pub struct ArticleSectionHeader {
        #[template_child]
        pub date_label: TemplateChild<Label>,
        pub g_date: Cell<GDateTime>,
    }

    impl Default for ArticleSectionHeader {
        fn default() -> Self {
            ArticleSectionHeader {
                date_label: TemplateChild::default(),
                g_date: Cell::new(Utc::now().naive_utc().into()),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleSectionHeader {
        const NAME: &'static str = "ArticleSectionHeader";
        type ParentType = gtk4::Box;
        type Type = super::ArticleHeader;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleSectionHeader {}

    impl WidgetImpl for ArticleSectionHeader {}

    impl BoxImpl for ArticleSectionHeader {}
}

glib::wrapper! {
    pub struct ArticleHeader(ObjectSubclass<imp::ArticleSectionHeader>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for ArticleHeader {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ArticleHeader {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn bind_model(&self, article: &ArticleGObject) {
        let imp = self.imp();

        if article.date() != imp.g_date.get() {
            imp.g_date.set(article.date());

            let date_str = DateUtil::format_date(&article.date().into());
            imp.date_label.set_text(&date_str);
        }
    }
}
