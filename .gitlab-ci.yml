stages:
  - fmt
  - version
  - lint
  - build
  - dist
  - upload
  - release

variables:
    APP_ID: "io.gitlab.news_flash.NewsFlash"
    DEVEL_APP_ID: "io.gitlab.news_flash.NewsFlash.Devel"
    FLATPAK_MODULE: "newsflash"
    MANIFEST_PATH: "build-aux/${DEVEL_APP_ID}.json"

fmt:
    stage: fmt
    tags:
        - docker
    image: rust:slim
    before_script:
    - rustup component add rustfmt
    script:
        # Create blank versions of our configured files
        # so rustfmt does not yell about non-existent files or completely empty files
        - echo -e "" >> src/config.rs
        - rustc --version && cargo --version
        - cargo fmt -- --check

gitversion:
    stage: version
    tags:
        - docker
    image:
        name: gittools/gitversion:6.0.0-alpine.3.17-7.0
        entrypoint: [""]
    variables:
        GIT_STRATEGY: fetch
    script: |
        /tools/dotnet-gitversion /output buildserver

        echo "Exporting some of these to dotenv files for variable usage in the pipeline and subsequent jobs..."
        grep 'GitVersion_MajorMinorPatch=' gitversion.properties >> gitversion.env
    artifacts:
        reports:
            dotenv: gitversion.env

lint:
    stage: lint
    tags:
        - docker
    image: quay.io/gnome_infrastructure/gnome-runtime-images:gnome-45
    script:
        # input generated version into meson project file
        - sed -i "s/0.0.0/${GitVersion_MajorMinorPatch}/g" meson.build
        - flatpak-builder --keep-build-dirs --user --disable-rofiles-fuse --stop-at=${FLATPAK_MODULE} flatpak_app --repo=repo ${BRANCH:+--default-branch=$BRANCH} ${MANIFEST_PATH}
        - echo "cargo clippy -- -D warnings" | flatpak-builder --disable-rofiles-fuse --build-shell=${FLATPAK_MODULE} flatpak_app ${MANIFEST_PATH}
    cache:
        paths:
            - cargo/
            - target/


flatpak:
    image: quay.io/gnome_infrastructure/gnome-runtime-images:gnome-45
    stage: build
    tags:
        - docker
    variables:
        BUNDLE: ${DEVEL_APP_ID}-${GitVersion_MajorMinorPatch}-${CI_COMMIT_SHORT_SHA}.flatpak
    script:
        - echo $BUNDLE
        # input generated version into meson project file & Cargo.toml
        - sed -i "s/0.0.0/${GitVersion_MajorMinorPatch}/g" meson.build
        - sed -i "s/0.0.0/${GitVersion_MajorMinorPatch}/g" Cargo.toml
        - flatpak-builder --stop-at=${FLATPAK_MODULE} app ${MANIFEST_PATH}
        - flatpak-builder --run app ${MANIFEST_PATH} meson setup --prefix=/app -Dprofile=development _build
        - flatpak-builder --run app ${MANIFEST_PATH} ninja -C _build install

        # Create a flatpak bundle
        - flatpak-builder --finish-only app ${MANIFEST_PATH}
        - flatpak build-export repo app
        - flatpak build-bundle repo ${BUNDLE} ${DEVEL_APP_ID}
    artifacts:
        paths:
            - ${BUNDLE}
        expire_in: 5 days
    cache:
        key: "flatpak"
        paths:
            - .flatpak-builder/git/
            - _build/target/

dist:
    stage: dist
    image: quay.io/gnome_infrastructure/gnome-runtime-images:gnome-45
    tags:
        - docker
    only:
        - stable
    script:
        # input generated version into meson project file & Cargo.toml
        - sed -i "s/0.0.0/${GitVersion_MajorMinorPatch}/g" meson.build
        - sed -i "s/0.0.0/${GitVersion_MajorMinorPatch}/g" Cargo.toml
        - flatpak-builder --stop-at=${FLATPAK_MODULE} app ${MANIFEST_PATH}
        - flatpak-builder --run app ${MANIFEST_PATH} meson setup --prefix=/app _build
        - flatpak-builder --run app ${MANIFEST_PATH} git config --global --add safe.directory /builds/news-flash/news_flash_gtk
        - flatpak-builder --run app ${MANIFEST_PATH} git config user.email "jangernert@gmail.com"
        - flatpak-builder --run app ${MANIFEST_PATH} git config user.name "CI Pipeline"
        - flatpak-builder --run app ${MANIFEST_PATH} git commit --allow-empty -a -m "set version to ${GitVersion_MajorMinorPatch}"
        - flatpak-builder --run app ${MANIFEST_PATH} meson dist -C _build --no-tests --allow-dirty
    artifacts:
        paths:
            - _build/meson-dist/
        expire_in: 5 days

upload:
    stage: upload
    tags:
        - docker
    only:
        - stable
    image: curlimages/curl:latest
    variables:
        CHECKSUM: "${FLATPAK_MODULE}-${GitVersion_MajorMinorPatch}.tar.xz.sha256sum"
        ARCHIVE: "${FLATPAK_MODULE}-${GitVersion_MajorMinorPatch}.tar.xz"
        PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/newsflash/${GitVersion_MajorMinorPatch}"
    script:
        - |
            curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "_build/meson-dist/${CHECKSUM}" "${PACKAGE_REGISTRY_URL}/${CHECKSUM}"
        - |
            curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "_build/meson-dist/${ARCHIVE}" "${PACKAGE_REGISTRY_URL}/${ARCHIVE}"


release:
    stage: release
    tags:
        - docker
    only:
        - stable
    image: registry.gitlab.com/gitlab-org/release-cli:latest
    variables:
        CHECKSUM: "${FLATPAK_MODULE}-${GitVersion_MajorMinorPatch}.tar.xz.sha256sum"
        ARCHIVE: "${FLATPAK_MODULE}-${GitVersion_MajorMinorPatch}.tar.xz"
        PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/newsflash/${GitVersion_MajorMinorPatch}"
    script:
        - |
            release-cli create --name "Release v.${GitVersion_MajorMinorPatch}" --tag-name "v.${GitVersion_MajorMinorPatch}" \
            --assets-link "{\"name\":\"${CHECKSUM}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${CHECKSUM}\"}" \
            --assets-link "{\"name\":\"${ARCHIVE}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${ARCHIVE}\"}"