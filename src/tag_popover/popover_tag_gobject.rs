use crate::util::constants;
use glib::{Object, ObjectExt, ParamSpec, ParamSpecBoolean, ParamSpecString, ToValue, Value};
use gtk4::subclass::prelude::*;
use news_flash::models::{Tag, TagID};
use once_cell::sync::Lazy;
use std::cell::RefCell;
use std::sync::Arc;

mod imp {
    use super::*;

    pub struct PopoverTagGObject {
        pub id: RefCell<TagID>,
        pub title: RefCell<String>,
        pub color: RefCell<String>,
        pub assigned: RefCell<bool>,
    }

    impl Default for PopoverTagGObject {
        fn default() -> Self {
            Self {
                id: RefCell::new(TagID::new("")),
                title: RefCell::new("".into()),
                color: RefCell::new(constants::TAG_DEFAULT_COLOR.into()),
                assigned: RefCell::new(false),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PopoverTagGObject {
        const NAME: &'static str = "PopoverTagGObject";
        type Type = super::PopoverTagGObject;
    }

    impl ObjectImpl for PopoverTagGObject {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::builder("title").build(),
                    ParamSpecString::builder("color").build(),
                    ParamSpecBoolean::builder("assigned").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "title" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.title.replace(input);
                }
                "color" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    if input.trim().is_empty() {
                        self.color.replace(constants::TAG_DEFAULT_COLOR.into());
                    } else {
                        self.color.replace(input);
                    }
                }
                "assigned" => {
                    let input: bool = value.get().expect("The value needs to be of type `bool`.");
                    self.assigned.replace(input);
                }
                _ => unreachable!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "title" => self.title.borrow().to_value(),
                "color" => self.color.borrow().to_value(),
                "assigned" => self.assigned.borrow().to_value(),
                _ => unreachable!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct PopoverTagGObject(ObjectSubclass<imp::PopoverTagGObject>);
}

impl Default for PopoverTagGObject {
    fn default() -> Self {
        Object::new()
    }
}

impl PopoverTagGObject {
    pub fn from_model(model: &Tag, assigned: bool) -> Self {
        let gobject = Self::default();
        let imp = gobject.imp();

        imp.id.replace(model.tag_id.clone());
        imp.title.replace(model.label.clone());
        imp.color.replace(match &model.color {
            Some(color) => color.clone(),
            None => constants::TAG_DEFAULT_COLOR.into(),
        });
        imp.assigned.replace(assigned);

        gobject
    }

    pub fn title(&self) -> String {
        self.imp().title.borrow().clone()
    }

    pub fn set_title(&self, title: Arc<String>) {
        self.set_property("title", &*title);
    }

    pub fn color(&self) -> String {
        self.imp().color.borrow().clone()
    }

    pub fn set_color(&self, color: Option<Arc<String>>) {
        let color = match color {
            Some(color) => (*color).clone(),
            None => constants::TAG_DEFAULT_COLOR.into(),
        };
        self.set_property("color", color);
    }

    pub fn is_assigned(&self) -> bool {
        *self.imp().assigned.borrow()
    }

    pub fn set_assigned(&self, assigned: bool) {
        self.set_property("assigned", assigned);
    }

    pub fn tag_id(&self) -> TagID {
        self.imp().id.borrow().clone()
    }
}
